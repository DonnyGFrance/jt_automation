import unittest
from selenium import webdriver


class login(unittest.TestCase):
    def test_login(self):
        options = webdriver.ChromeOptions()
        options.add_argument('--allow-running-insecure-content')
        options.add_argument('--ignore-certificate-errors')
        options.add_argument("--start-maximized")
        driver = webdriver.Chrome(options=options)
        driver.get("https://www.mitrais.com/")
        driver.implicitly_wait(10)
        aboutUs = driver.find_element_by_xpath("//A[@href='https://www.mitrais.com/about-us/'][text()='About Us']")
        assert aboutUs
        aboutUs.click()
        driver.implicitly_wait(10)
        assert driver.find_element_by_xpath("//STRONG[text()='Mitrais Software Development And Software Product Services']")
